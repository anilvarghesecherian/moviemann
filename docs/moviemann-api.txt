Moviemann API

Available End Points
Home Page of the Moviemann API Request URL: http://localhost:7070/moviemann/api/

Params:
username: moviemann
password: boyinstripedpy

1. End point for searching a string in movies JSON

    2.a) Get movie titles and number of occurrences of a string
        Request URL: http://localhost:7070/moviemann/api/movies/{search string}
        
        Example:
        Searching for http://localhost:7070/moviemann/api/movies/adam gives the below output

    2.b) Get movies that contains the string searched for
        Request URL: http://localhost:7070/moviemann/api/movies/{search term}/list
        
        Example:
        Searching for http://localhost:7070/moviemann/api/movies/adam/list gives the below output
        [
            { "title": "Life of Adam", "year": 2018,
            "cast": [ "Adam Smith" ],
            "genres": [ "History" ] } 
        ]
2. End point for searching a string in movies JSON using recursion 
    
    Request URL: http://localhost:7070/moviemann/api/movies/json/recursion/{search string}
    
    Example:
    Searching for http://localhost:7070/moviemann/api/movies/json/recursion/adam gives the below output
    [ 
        "{\"cast\":[\"Adam Smith\"],\"year\":2018,\"genres\":[\"History\"],\"title\":\"Life of Adam\"}", "Adam Smith", "Life of Adam" 
    ]