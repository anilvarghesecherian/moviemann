package com.moviemann.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoviemannApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviemannApplication.class, args);
	}

}
