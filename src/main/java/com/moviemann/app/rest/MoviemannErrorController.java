package com.moviemann.app.rest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.GetMapping;

public class MoviemannErrorController implements ErrorController {
	
	@GetMapping("/error")
	public String erros() {
		
		StringBuilder SB = new StringBuilder();
		SB.append("<h2 style='background:lightgrey'><br><b>Welcome to Moviemann!!!</b></h2>")
		  .append("<hr>Moviemann is an api for seaching a JSON movie collection object. This api currently supports only string single string search.<br>")
		  .append("<h5>Use the end point <a href='http://localhost:7070/moviemann/api/'>http://localhost:7070/moviemann/api/</a> to get the home page of moviemann api.</h5>");
				  	
		return SB.toString();
	}

	@Override
	public String getErrorPath() {
		// TODO Auto-generated method stub
		return "/error";
	}

}
