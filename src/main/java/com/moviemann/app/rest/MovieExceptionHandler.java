package com.moviemann.app.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class MovieExceptionHandler {
	
	/* Methods to handle exceptions */
	@ExceptionHandler
	public ResponseEntity<MovieSearchErrorResponse> handleException(MovieNotFoundException e){
		
		MovieSearchErrorResponse error = new MovieSearchErrorResponse();
		
		error.setStatus(HttpStatus.NOT_FOUND.value());
		error.setMessage(e.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
		
		return new ResponseEntity<MovieSearchErrorResponse>(error, HttpStatus.NOT_FOUND);
		
	}
	
	@ExceptionHandler
	public ResponseEntity<MovieSearchErrorResponse> handleException(Exception e){
		
		MovieSearchErrorResponse error = new MovieSearchErrorResponse();
		
		error.setStatus(HttpStatus.BAD_REQUEST.value());
		error.setMessage("No result available " + e.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
		
		return new ResponseEntity<MovieSearchErrorResponse>(error, HttpStatus.BAD_REQUEST);
		
	}

}
