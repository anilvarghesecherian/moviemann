package com.moviemann.app.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.moviemann.app.Helper.MoviemannJsonHelper;
import com.moviemann.app.pojo.Movie;
import com.moviemann.app.service.MovieSearchService;

@RestController
@RequestMapping("/api")
public class MoviemannRestController {
	

	MovieSearchService movieService;
	
	@Autowired
	public MoviemannRestController(MovieSearchService movieService) {
		this.movieService = movieService;
	
	}
	
	
	@GetMapping("/")
	public String movieMann() {
		
		StringBuilder SB = new StringBuilder();
		SB.append("<h2 style='background:lightgrey'><br><b>Welcome to Moviemann!!!</b></h2>")
		  .append("<hr>Moviemann is an api for seaching a JSON movie collection object. This api currently supports only string single string search.<br>")
		  .append("<h5>Use the end point <a href='http://localhost:7070/moviemann/api/movies/adam'>http://localhost:7070/moviemann/api/movies/{searchTerm}</a> to get the list of all movie titles that contain the search string. This end point also gives the number of occurrences of the search string in the move json.</h5>"
				  	+ " <p><em>For example the hyperlink above yields all movie titles that contain the search string 'adam'.</p></em>")
		  .append("<hr><br>")
				  .append("<h5>Use the URL <a href='http://localhost:7070/moviemann/api/movies/adam/list'>http://localhost:7070/moviemann/api/movies/{searchTerm}/list</a> to get the list of all movies that contain the search string as a JSON array.</h5>"
						  	+ " <p><em>For example this hyperlink yields all movies that contain the search string 'adam' as JSON array.</em> You may use JSON formatter extension in google chrome browser to view formatted JSON.</P>");
				
		return SB.toString();
	}
	

	
	
	@GetMapping("/movies")
	public List<Movie> getMovies() throws IOException{
		return movieService.getMovies();
		
	}
	
	@GetMapping(path="/movies/{search}")
	public String getMovies(@PathVariable("search") String searchTerm) throws IOException{
		
		if(searchTerm.equalsIgnoreCase("*")) {
			throw new MovieNotFoundException("Please use mapping '/movies' to retrieve all movies instead of explicity specifying" + searchTerm);
		}
		
		movieService.setSearchTerm(searchTerm);
		return movieService.getFilteredTitles();
		
	}
	
	//Renders the filtered movie json based on search criteria
	@GetMapping(path="/movies/{search}/list")
	public ArrayList<Movie> filterMovies(@PathVariable("search") String searchTerm) throws IOException{
		
		if(searchTerm.equalsIgnoreCase("*")) {
			return movieService.getMovies();
		}
		
		movieService.setSearchTerm(searchTerm);
		
		ArrayList<Movie> filterdMovies = movieService.getFilteredMovies();
		

		if( filterdMovies!= null && filterdMovies.size() > 0) {
			return filterdMovies;
		}
		
		throw new MovieNotFoundException("No movies found matching your search for: " + searchTerm);
		
	}
	
	@GetMapping(path="/movies/json/recursion/{search}")
	public ArrayList<String> getMoviesByRecursion(@PathVariable("search") String searchTerm) throws IOException{
		
		if(searchTerm.equalsIgnoreCase("*")) {
			throw new MovieNotFoundException("Please use mapping '/movies' to retrieve all movies instead of explicity specifying" + searchTerm);
		}
		
		MoviemannJsonHelper moviemannJsonHelper = new MoviemannJsonHelper();
		
		moviemannJsonHelper.setSearchString(searchTerm);
		moviemannJsonHelper.searchJson();
		return moviemannJsonHelper.getFilteredJson();
		
	}
	
	
}
