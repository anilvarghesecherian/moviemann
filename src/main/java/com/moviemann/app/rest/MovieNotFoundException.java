package com.moviemann.app.rest;

public class MovieNotFoundException extends RuntimeException {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -1569266639655505839L;

	public MovieNotFoundException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public MovieNotFoundException(String message) {
		super(message);
		
	}

	public MovieNotFoundException(Throwable cause) {
		super(cause);
		
	}
	
	

}
