package com.moviemann.app.Helper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MoviemannJsonHelper {
	
	private JSONParser jParser;
	private JSONObject jObject;
	private JSONArray jArray;
	private ArrayList<String> filteredJson;
	private String searchString;
	
	
	public MoviemannJsonHelper() {
		this.jParser = new JSONParser();
		this.filteredJson = new ArrayList<String>();
	}
	
	public ArrayList<String> getFilteredJson() {
		return filteredJson;
	}



	public void setFilteredJson(ArrayList<String> filteredJson) {
		this.filteredJson = filteredJson;
	}



	public String getSearchString() {
		return searchString;
	}



	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}



	public MoviemannJsonHelper(String filePath) throws FileNotFoundException, IOException, ParseException {
		this();
		Object object = this.jParser.parse(new FileReader(filePath));
		
		if (object.getClass() == JSONArray.class) {
			this.jArray = (JSONArray) object;
			
		} else if(object.getClass() == JSONObject.class) {
			this.jObject = (JSONObject) object;
			
		}
		
	}
	
	public ArrayList<String> searchJson() {
		try {
			
			long startTime = System.currentTimeMillis();
			MoviemannJsonHelper mjh = new MoviemannJsonHelper("data/movies.json");
			
			for (int i = 0; i < mjh.jArray.size(); i++) {
				JSONObject jObj = (JSONObject) mjh.jArray.get(i);
				searchInJson(jObj, this.searchString);
			}
			System.out.println("Time taken for execution(ms): " + (System.currentTimeMillis() - startTime));
			return filteredJson;
			
			
		
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * A recursive method to search for a specific string in a JSON object, JSON
	 * array or JSON String
	 */
	private void searchInJson(Object object, String value) {
		
		if (object.getClass() == JSONObject.class) {
			
			JSONObject jsonObject = (JSONObject) object;
			
			if(jsonObject.toString().toLowerCase().contains(value.toLowerCase())) {
				filteredJson.add(object.toString());
			}
			
			jsonObject.keySet().forEach(key -> {
		    	
				Object child = jsonObject.get(key);
		    	
				searchInJson(child, value);

		    });
			
		} else if(object.getClass() == JSONArray.class) {
			JSONArray jsonArray = (JSONArray) object;
			
			for (int i = 0; i < jsonArray.size(); i++) {
				Object jObj = jsonArray.get(i);
				
				searchInJson(jObj, value);
			}
		}	else if(object.getClass() == String.class)	    {
			if (object.toString().toLowerCase().contains(value.toLowerCase())){
				filteredJson.add(object.toString());
			}
		}
	}
}


