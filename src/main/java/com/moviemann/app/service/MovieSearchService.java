package com.moviemann.app.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moviemann.app.pojo.Movie;

@Service
public class MovieSearchService {
	
	private ArrayList<String> filteredTitles;
	private ArrayList<Movie> movies;
	private String searchTerm;
	private ArrayList<Movie> filteredMovies;
	private int resultCnt = 0;
	
	public MovieSearchService() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		this.movies = mapper.readValue(new File("data/movies.json"), new TypeReference<List<Movie>>(){});
	}
	
	public MovieSearchService(String searchTerm) throws JsonParseException, JsonMappingException, IOException {
		this();
		this.searchTerm = searchTerm;
		
		
	}
	
	
	public ArrayList<Movie> getFilteredMovies() {
		this.filteredMovies = new ArrayList<Movie>();
		
		processSearch();
	
		LinkedHashSet<Movie> filteredMovieSet = new LinkedHashSet<Movie>();
		filteredMovieSet.addAll(filteredMovies);
		filteredMovies.clear();
		
		filteredMovies.addAll(filteredMovieSet);
		return filteredMovies;
	}

	public void setFilteredMovies(ArrayList<Movie> filteredMovies) {
		this.filteredMovies = filteredMovies;
	}

	public String getFilteredTitles() {
		long startTime = System.currentTimeMillis();
		
		this.resultCnt = 0;
		
		/* Reset Filtered Titles and Filtered Movies list */
		if (filteredTitles == null || filteredTitles.size() > 0) {
			filteredTitles = new ArrayList<String>();
			
		}
		
		if(filteredMovies == null || filteredMovies.size() > 0) {
			filteredMovies = new ArrayList<Movie>();
		}
			
		
		processSearch();
		
		this.resultCnt = filteredTitles.size();
		System.out.println("Search String occurances: " + this.resultCnt);
		
		System.out.println("Time Taken for exection(ms): " + (System.currentTimeMillis() - startTime));
		
		return output();
		
	}
	

	
	public void processSearch() {
		movies.forEach(movie -> search(movie));	
		this.resultCnt = filteredTitles.size();
		
	}

	public void search(Movie movie) {
		
		if (filteredTitles == null) {
			filteredTitles = new ArrayList<String>();
		}
		
		if(filteredMovies == null) {
			filteredMovies = new ArrayList<Movie>();
		}
		
		String title = movie.getTitle();
						
		/* Check if the cast contains the search term */
		movie.getCast().forEach(actor -> check(movie, searchTerm, actor, title));
		
		/* Check if the Genres contain the search term */
		movie.getGenres().forEach(genre -> check(movie, searchTerm, genre, title));
		
		/* check if the search term is present in the title */
		if (title.toLowerCase().contains(searchTerm.toLowerCase())) {
			filteredTitles.add(title);
			filteredMovies.add(movie);
		}
		
		/* check if the search term is present in year */
		Integer year = null;
		
		try {
			year = Integer.valueOf(searchTerm);
			if(movie.getYear() == year) {
				filteredTitles.add(title);
				filteredMovies.add(movie);
			}
		} catch (Exception e) {
			
		}
	}

	private boolean check(Movie movie, String searchTerm, String searchInString, String title) {
		if(searchInString.toLowerCase().contains(searchTerm.toLowerCase())){
			
			filteredTitles.add(title);
			filteredMovies.add(movie);
			return true;
		}
		
		return false;
			
	}
	

	public ArrayList<Movie> getMovies() {
		return movies;
	}

	public void setMovies(ArrayList<Movie> movies) {
		this.movies = movies;
	}

	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}
	
	

	public int getResultCnt() {
		return resultCnt;
	}

	public void setResultCnt(int resultCnt) {
		this.resultCnt = resultCnt;
	}

	public String output() {
		
		StringBuilder SB = new StringBuilder();
		
		SB.append("<b>Search String</b>: ").append(searchTerm).append("<hr>");
		
		SB.append("<br>").append("Occurrence(s): ").append(filteredTitles.size());
		
		LinkedHashSet<String> filteredTitleSet = new LinkedHashSet<String>();
		filteredTitleSet.addAll(filteredTitles);
		filteredTitles.clear();
		
		filteredTitles.addAll(filteredTitleSet);
		
		return (SB.append("<br>")).append("Title(s): ")
				.append(System.getProperty("line.separator")).append(filteredTitleSet).toString();
	}	
	
	

}
