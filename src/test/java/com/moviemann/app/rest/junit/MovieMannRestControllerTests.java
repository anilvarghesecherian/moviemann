package com.moviemann.app.rest.junit;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.moviemann.app.MoviemannApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MoviemannApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class MovieMannRestControllerTests {
	
	
	@Value("${spring.security.user.name}")
	String user;
	
	@Value("${spring.security.user.password}")
	String password;
	
	@LocalServerPort
	private int randomServerPort;
	
			
	HttpHeaders headers = new HttpHeaders();
	
	//check the end point
	@Test
	public void GivenMoviesWhenGetMoviesReturnsMovieList() throws Exception {
		TestRestTemplate trt = new TestRestTemplate(user, password);
		ResponseEntity<String> response = trt.getForEntity(generateBaseUrl(randomServerPort) + "/api/", String.class);
		assertTrue(response.getStatusCode() == HttpStatus.OK);
		
	}
	
	
	
	//Check movie search returns the right title
	@Test
	public void GivenMovieCheckForASpecificTitileRetrunsTrue() throws Exception{
		TestRestTemplate trt = new TestRestTemplate(user, password);
		ResponseEntity<String> response = trt.getForEntity(generateBaseUrl(randomServerPort) + "/api/movies/adam", String.class);
		assertTrue(response.getBody().contains("Life of Adam"));
	}
	
	
	@Test
	public void GivenSearchStringCheckForMoviesByTitleContainingItRetrunsTrue() throws Exception{
		TestRestTemplate trt = new TestRestTemplate(user, password);
		ResponseEntity<String> response = trt.getForEntity(generateBaseUrl(randomServerPort) + "/api/movies/action/list", String.class);
		String responseBody = response.getBody();
		assertTrue(responseBody.contains("Avengers: Age of Ultron") && 
				   responseBody.contains("The Avengers"));
	}
	
	private String generateBaseUrl(int port) {
		
		return "http://localhost:" + port + "/moviemann";
	}

}
