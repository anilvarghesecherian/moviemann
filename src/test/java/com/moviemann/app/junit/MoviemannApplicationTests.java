package com.moviemann.app.junit;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moviemann.app.MoviemannApplication;
import com.moviemann.app.pojo.Movie;
import com.moviemann.app.service.MovieSearchService;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MoviemannApplication.class)
public class MoviemannApplicationTests {
	
	@Autowired
	MovieSearchService movieService;
	
	@Test
	public void WhenSearchStringExistsInJsonReturnsFilterdCollection() {
		
		/*
		 * Given search term history and the movie json file has only one movie that
		 * contains the term history and the title is Life of Adam
		 */
		
		String searchTerm = "History";
		String movieTitle = "Life of Adam";
		
		//When
		ArrayList<Movie> filteredMovieList = filterMovies(searchTerm);
		int resCnt = getSearchStringCnt(searchTerm);
		
		//Then
		assertTrue(filteredMovieList.size() == 1);
		assertTrue(filteredMovieList.get(0).getTitle().equalsIgnoreCase(movieTitle));
		assertTrue(resCnt == 1);
		
	}

	
	@Test
	public void WhenSearchStringDoesNotExistsInJsonReturnsColletionOfSize0() {
		
		/*
		 * Given search term Harvest and the movie json file has no movies that
		 * contain the term.
		 */
		
		String searchTerm = "Harvest";
		
		//When
		ArrayList<Movie> filteredMovieList = filterMovies(searchTerm);
		int resCnt = getSearchStringCnt(searchTerm);
		
		//Then
		assertTrue(filteredMovieList.size() == 0);
		assertTrue(resCnt == 0);
		
	}
	
	/*
	 * Input: "Action" Output: Search Term: "Action" Movies:
	 * "Avengers: Age of Ultron", "The Avengers" count: 2 Example 2
	 */
	
	@Test
	public void WhenSearchForActionReturnsTwoMovieTitles() {
		
		//Given
		String searchTerm = "Action";
		
		//When
		ArrayList<Movie> filteredMovieList = filterMovies(searchTerm);
		int resCnt = getSearchStringCnt(searchTerm);
		
		//Then
		assertTrue(filteredMovieList.size() == 2);
		assertTrue(resCnt == 2);
		
	}



	/*
	 * Input: "Jeremy Renner" Output: Search Term: "Jeremy Renner" Movie:
	 * "The Avengers" count: 1 Example 3
	 */
	
	@Test
	public void WhenSearchForJeremyRennerReturnsTwoMovieTitles() {
		//Given
		String searchTerm = "Jeremy Renner";
		String movieTitle = "The Avengers";
			
		//When
		ArrayList<Movie> filteredMovieList = filterMovies(searchTerm);
		int resCnt = getSearchStringCnt(searchTerm);
		
		//Then
		assertTrue(filteredMovieList.size() == 1);
		assertTrue(filteredMovieList.get(0).getTitle().equalsIgnoreCase(movieTitle));
		assertTrue(resCnt == 1);
	}


	/*
	 * Input: "Chris" Output: Search Term: "Chris" Movie: "Avengers: Age of Ultron",
	 * "The Avengers" count: 4 Example 4
	 */
	
	@Test
	public void WhenSearchForChrisReturnsTwoMovieTitles() {
		
		//Given
			String searchTerm = "Chris";
				
		//When
		ArrayList<Movie> filteredMovieList = filterMovies(searchTerm);
		int resCnt = getSearchStringCnt(searchTerm);
		
		//Then
		assertTrue(filteredMovieList.size() == 2);
		assertTrue(resCnt == 4);
	}


	/* Input: "Adam" Output: Search Term: "Adam" Movie: "Life of Adam" count: 2 */
	@Test
	public void WhenSearchForAdamReturnsOneMovieTitle() {
		//Given
		String searchTerm = "Adam";
			
		//When
		ArrayList<Movie> filteredMovieList = filterMovies(searchTerm);
		int resCnt = getSearchStringCnt(searchTerm);
		//Then
		assertTrue(filteredMovieList.size() == 1);
		assertTrue(resCnt == 2);
		
	}
	
	private ArrayList<Movie> filterMovies(String searchTerm) {
		movieService.setSearchTerm(searchTerm);	
		return movieService.getFilteredMovies();
	}
	
	private int getSearchStringCnt(String searchTerm) {
		movieService.setSearchTerm(searchTerm);	
		movieService.getFilteredTitles();
		return movieService.getResultCnt();
		
	}
	

}
